class TextStorage:
    """Класс для чтения и хранения текста."""

    def __init__(self, path_to_text: str):
        self.lines: list = self._read_lines_from_text(path_to_text)

    def _read_lines_from_text(self, path_to_text: str) -> list:
        with open(path_to_text) as text_file:
            lines = text_file.readlines()
            return [line.rstrip() for line in lines]


class RussianPhoneChecker:
    """Класс, реализующий проверку на номер РФ."""

    def check(self, phone: str) -> bool:
        """Метод проверки телефонного номера."""
        is_russian_phone = False
        if phone.startswith('+'):
            phone = phone[1:]
        if not phone.isdigit():
            is_russian_phone = False
        if phone.startswith('79'):
            is_russian_phone = True
        self._log_result(is_russian_phone)
        return is_russian_phone

    def _log_result(self, is_russian_phone: bool):
        if is_russian_phone:
            print('Телефонный номер является номером РФ.')
        else:
            print('Телефонный номер не является номером РФ.')


if __name__ == '__main__':
    checker = RussianPhoneChecker()
    print(checker.check('+79529220099'))
