# Проект с зависимостями линтера wemake-python-styleguide

### Подготовка
`pip install -r requirements.txt`

или c помощью Poetry

`poetry install -E lint`

внутри виртуального окружения

### Запуск
`flake8 entry.py` или
`flake8 /path/to/file`


### Официальная документация
[Ссылка на официальную документация](https://wemake-python-stylegui.de/en/latest/index.html)

[Ссылка на репозиторий GitHub](https://github.com/wemake-services/wemake-python-styleguide)


### Установка Poetry
[Официальная документация инструмента](https://python-poetry.org/)
Установить в глобальный Python
